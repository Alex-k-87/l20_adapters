<?php


namespace App;


interface AdapterInterface
{
 public function parse($ip);
 public function getCityName();
 public function getCountryCode();

}
