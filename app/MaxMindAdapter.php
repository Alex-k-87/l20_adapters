<?php


namespace App;


class MaxMindAdapter implements AdapterInterface
{
    protected $reader;
    protected $record;

    public function __construct(\GeoIp2\Database\Reader $reader)
    {
        $this->reader = $reader;
    }

    public function getCityName()
    {
        return $this->record->city->name;
    }

    public function getCountryCode()
    {
        return $this->record->country->code;
    }

    public function parse($ip)
    {
        // TODO: Implement parse() method.
        try {
            $this->record = $this->reader->city($ip);
        } catch (\GeoIp2\Exception\AddressNotFoundException $exception) {
            $this->record = $this->reader->city(env('DEFAULT_IP_ADDR'));
        }
    }
}
