<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


App::singleton(\App\AdapterInterface::class, function () {
//    $reader = new \GeoIp2\Database\Reader(resource_path() . '/GeoLite2/GeoLite2-City.mmdb');
//    return new \App\MaxMindAdapter($reader);

    return new \App\IpapiAdapter();
});

Route::get('/r/{code}', function ($code, \App\AdapterInterface $adapter) {

    dd($adapter);

    $link = \App\Link::where('short_code', $code)->get()->first();
    $adapter->parse(request()->ip());

    $statistic = new \App\Statistic();
    $statistic->id = \Ramsey\Uuid\Uuid::uuid4()->toString();
    $statistic->link_id = $link->id;
    $statistic->ip = request()->ip();
    $statistic->user_agent = request()->userAgent();
    $statistic->country_code = $adapter->getCountryCode();
    $statistic->city_name = $adapter->getCityName();
    $statistic->save();

    return redirect($link->source_link);
});
